import { getGasPrice } from './utils'
import info from './info.json'
import { ethers } from 'hardhat'

async function main() {
  //Set the gasLimit to 3e5 (a safe overestimate) and get the gas price.
  const gasLimit = 3e5
  const gasPrice = await getGasPrice()
  const executableSampleSourceAddress = info.ropsten.ExecutableSample
  const executableSampleDestAddress = info.neartest.ExecutableSample
  const msg = 'Hello...'

  console.log(
    'value in ether',
    ethers.utils.formatEther(Math.floor(gasLimit * gasPrice).toString()),
  )

  const ExecutableSampleSource = await ethers.getContractFactory(
    'ExecutableSample',
  )
  const executableSample = ExecutableSampleSource.attach(
    executableSampleSourceAddress,
  )

  const tx = await executableSample.setRemoteValue(
    'aurora',
    executableSampleDestAddress,
    msg,
    {
      value: BigInt(Math.floor(gasLimit * gasPrice)),
    },
  )
  console.log(tx)

  await tx.wait()
}

main().catch((error) => {
  console.error(error)
  process.exitCode = 1
})
