import hre, { ethers } from 'hardhat'
import info from './info.json'

async function main() {
  const network = hre.network.name as 'ropsten' | 'neartest'

  const networkInfo = info[network]

  const ExecutableSample = await ethers.getContractFactory('ExecutableSample')
  const executableSample = await ExecutableSample.deploy(
    networkInfo.gateway,
    networkInfo.gasReceiver,
  )

  await executableSample.deployed()

  console.log(`ExecutableSample deployed to ${executableSample.address}`)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error)
  process.exitCode = 1
})
