import axios from 'axios'

export const getGasPrice = async () => {
  const api_url = 'https://devnet.api.gmp.axelarscan.io'

  const requester = axios.create({ baseURL: api_url })
  const params = {
    method: 'getGasPrice',
    destinationChain: 'Aurora',
    sourceChain: 'Ethereum',
    sourceTokenSymbol: 'ETH',
  }

  // send request
  const response = await requester.get('/', { params }).catch((error) => {
    return { data: { error } }
  })
  const result = response.data.result

  console.log(result)

  const dest = result.destination_native_token
  const destPrice = 1e18 * dest.gas_price * dest.token_price.usd
  console.log(destPrice / result.source_token.token_price.usd)

  return destPrice / result.source_token.token_price.usd
}
