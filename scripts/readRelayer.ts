import info from './info.json'
import { ethers } from 'hardhat'

async function main() {
  const executableSampleDestAddress = info.neartest.ExecutableSample

  const ExecutableDestSource = await ethers.getContractFactory(
    'ExecutableSample',
  )
  const executableSample = ExecutableDestSource.attach(
    executableSampleDestAddress,
  )

  const msg = await executableSample.value()

  console.log({ msg })
}

main().catch((error) => {
  console.error(error)
  process.exitCode = 1
})
